"""
Usando os mesmos inputs do código anterior, reconstrua o problema de maneira
declarativa.
Funções que pode te ajudar:
- operator.mul
- map()
"""

lista1 = ['banana', 'maca', 'morango']
lista2 = list((valor * 2 for valor in lista1))
print(lista2)