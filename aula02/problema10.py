"""
Especifique uma função que receba um número uma string e retorne se na string
há o mesmo número de elementos que foram passados no parâmetro
Ex:
f(3, ‘aaa’) -> True
f(10, ‘Batata’) -> False
"""

def bool(num:int, string:str):
    return print(len(string) == num)

bool(4, 'jess')