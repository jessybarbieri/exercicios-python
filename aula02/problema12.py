"""
Faça uma função que receba um número, caso esse número seja múltiplo de 3,
retorne “queijo”, caso contrário, retorne o valor de entrada.
EX:
f(5) -> 5
f(3) -> ‘queijo’
f(6) -> ‘queijo’
"""

def mul_3(num: int):
    if num % 3 == 0:
        return print('queijo')

    else:
        return print(num)

num = int(input('Digite um número: '))
mul_3(num)

