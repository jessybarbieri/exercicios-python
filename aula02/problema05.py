"""
Faça um programa que itere em uma lista de maneira imperativa e que armazene
em uma nova lista seu valor processado por uma função.
EX:
entrada = [‘foo’, ‘bar’, ‘spam’, ‘eggs’]
função = f(x) = x * 2
saida = [‘foofoo’, ‘barbar’, ‘spamspam’, ‘eggseggs’]
"""

lista1 = ['banana', 'maca', 'morango']
lista2 = []

for valor in lista1:
    valor = valor * 2
    lista2.append(valor)

print(lista2)