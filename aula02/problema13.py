"""
Faça uma função que receba um número, caso esse número seja múltiplo de 5,
retorne “goiabada”, caso contrário, retorne o valor de entrada.
EX:
f(5) -> ‘goiabada’
f(3) -> 3
f(10) -> ‘goiabada’
"""

def mul_5(num: int):
    if num % 5 == 0:
        return print('goiabada')

    else:
        return print(num)

num = int(input('Digite um número: '))
mul_5(num)
