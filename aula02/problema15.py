"""
Faça uma função que se aplique uma função duas vezes em um valor passado
EX:
reaplica(soma_2, 2) -> 6
reaplica(sub_2, 2) -> -2
"""

def twice (f: callable, val: int):
    return f(f(val))

def soma_2(f: callable, x: int)
    return x + x

twice(soma_2, 2)
