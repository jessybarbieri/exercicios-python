"""
Faça uma função que não tenho a cláusula de retorno e mostre seu tipo
"""

def func(x: int):
    print(type(x))

func(5)