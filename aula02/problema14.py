"""
Faça uma função que receba um número, caso esse número seja múltiplo de 3 e
5, retorne “romeu e julieta”, caso contrário, retorne o valor de entrada.
EX:
f(3) -> 3
f(5) -> 5
f(15) -> ‘romeu e juleita’
"""

def multiplo(num: int):
    if num % 3 == 0 and num % 5 == 0:
        return print('romeu e julieta')

    else:
        return print(num)

num = int(input('Digite um número: '))
multiplo(num)