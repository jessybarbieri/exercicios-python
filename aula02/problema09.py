"""
Crie uma função que faça uma saudação a alguém. A função deve receber dois
argumentos ‘saudação’ e ‘nome’.
Ex:
f(‘Ahoy’, ‘Fausto’) -> ‘Ahoy Fausto’
f(‘Olá’, bb’) -> Olá bb’
"""

def saudacao(nome:str, saudacao:str):
    print(f'{saudacao} {nome}')

saudacao('jessy', 'oi')