"""
Resolva de maneira imutável (Ou seja, retornando uma nova lista)
Ex:
Entrada = [1, 2, 3]
Saída = [3, 2, 1]
Coisas que podem te ajudar:
list.insert, list.append, list.remove
"""

lista1 = [1, 2, 3]
lista2 = []

for valor in lista1:
    lista2.insert(0, valor)

print(lista2)
