"""
Faça um programa que receba uma data de nascimento (15/07/87) e imprima
‘Você nasceu em <dia> de <mes> de <ano>’
"""

data = str('15/07/87')
data = data.split('/')

print('você nasceu em {dia} de {mes} de {ano}'.format(dia=data[0], mes=data[1], ano=data[2]))