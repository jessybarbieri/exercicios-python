"""
Faça um programa para uma loja de tintas. O programa deverá pedir o tamanho
em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é
de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18
litros, que custam R$ 80,00. Informe ao usuário a quantidades de latas de tinta a
serem compradas e o preço total.
"""
import math

tamanho = int(input('informe o tamanho em metros quadrados, da area a ser pintada: '))
litros = tamanho / 3

precoLitro = 80
capacidadeLitro = 18

if (litros <= 18):
    litros = 18

latas = litros / capacidadeLitro
total = latas * precoLitro

print(f'Você usará {math.ceil(latas)} latas de tinta')
print(f'O preco total é de: R${math.ceil(total)}')
