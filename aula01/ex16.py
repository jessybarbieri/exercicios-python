"""
Faça um programa, com uma função, que calcula a média de uma lista.
Funções embutidas que podem te ajudar:
-len(lista) -> calcula o tamanho da lista
-sum(lista) -> faz o somatório dos valores
"""

def calc_media(x):
    tam = len(x)
    soma = sum(x)
    media = soma / tam

    print(media)

calc_media([5, 5, 5, 5, 5])