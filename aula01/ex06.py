"""
Faça um programa que pergunte o preço de três produtos e informe qual produto
você deve comprar, sabendo que a decisão é sempre pelo mais barato.
"""

v1 = float(input('Digite o 1º preço: '))
v2 = float(input('Digite o 2º preço: '))
v3 = float(input('Digite o 3º preço: '))

if v1 < v2 and v1 < v3:
    print('compre o 1º produto')

elif v2 < v1 and v2 < v3:
    print('compre o 2º produto')

elif v3 < v1 and v3 < v2:
    print('compre o 3º produto')

elif v1 == v2 and v1 and v2 < v3:
    print('O 1º e 2º produtos são os mais baratos')

elif v1 == v3 and v1 and v3 < v2:
    print ('O 1º e 3º produtos são os mais baratos')
        
elif v2 == v3 and v2 and v3 < v1:
    print ('O 2º e 3º produtos são os mais baratos')

else:
    print('todos os preços são iguais')