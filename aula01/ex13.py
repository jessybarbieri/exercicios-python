"""
Faça um programa que: Dada uma lista [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] e um número
inteiro, imprima a tabuada desse número.
"""

num = int(input('Digite um número: '))
l2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for valor in l2:
    res = valor * num
    print(f'{valor} * {num} = {res}')
