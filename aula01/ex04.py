"""
Faça um Programa que peça 2 números inteiros e um número float. Calcule e
mostre:
-O produto do dobro do primeiro com metade do segundo .
-A soma do triplo do primeiro com o terceiro.
-O terceiro elevado ao cubo.
"""

n1 = int(input('Digite um numero inteiro: '))
n2 = int(input('Digite outro numero inteiro: '))
n3 = float(input('Digite um numero flutuante: '))

produto = ((n1 * 2) * (n2 / 2))
soma = (n1 * 3) + n3
cubo = n3**3

print (f'Produto: {produto}')
print (f'Soma: {soma}')
print (f'Cubo: {cubo}')
