"""
Faça um programa, com uma função, que calcula a mediana de uma lista.
Funções embutidas que podem te ajudar:
-sorted(lista) -> ordena a lista
"""

def mediana(x):
    lista = sorted(x)
    n = len(lista)
    index = n // 2

    if n % 2:
        print(lista[index])
    
    else:
        print(sum(lista[index - 1 : index + 1]) / 2)

mediana([2, 4, 6, 8, 10])