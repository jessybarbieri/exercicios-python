"""
Faça um programa que leia 5 números e informe o maior número.
"""

cont = 0
aux= -999999

while cont <= 4:
    numero = float(input(f'Digite o numero {cont}: '))
    aux = numero if numero > aux else aux
    cont += 1
    
print(f'maior numero: {aux}')