from unittest import TestCase
from unittest import mock
import problema04

class TestConverteStringEmLista(TestCase):

    def test_verifica_se_a_função_converte_string_em_lista_existe(self):
        self.assertEqual(
            hasattr(problema04, 'converte_string_em_lista'),
            True,
            'função converte_string_em_lista não existe'
        )

    def test_verifica_se_a_função_converte_string_em_lista_é_chamavel(self):
        self.assertEqual(
            hasattr(problema04.converte_string_em_lista, '__call__'),
            True,
            'função_converte_string_em_lista não é chamavel'
        )

    def test_verifica_se_a_função_converte_string_em_lista_refecebe_parâmetro(self):
        self.assertEqual(
            problema04.converte_string_em_lista.__code__.co_argcount,
            1,
            'função converte_string_em_lista não recebe nenhum parâmetro'
        )

    def test_função_converte_string_em_lista_deve_receber_uma_frase(self):
        fake_file = mock.MagicMock()
        
