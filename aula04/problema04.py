"""
Crie uma função que recebe uma string e a transforma em uma lista:
‘Python’ -> [‘Python’]
‘Python love’ -> [‘Python’, ‘love’]
‘Python é foda’ -> [‘Python’, ‘é’, ‘foda’]
"""

def converte_string_em_lista(string):
    ...