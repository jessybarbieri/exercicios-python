from behave import when, then
from problema3 import fizzbuzz

@then('{numero:d} é {resultado}')
def checa_retorno_de_fizzbuzz(context, numero, resultado):
    assert context.response == resultado

@when('o FizzBuzz for chamado com o número {numero:d}')
def chamar_fizzbuzz_com_numero(context, numero):
    context.response = fizzbuzz(numero)