# Numeros divisiveis por 3 aparecer Fizz no lugar do numero
# Numeros diviseis por 5 aparecer Buzz no lugar do numero
# Numeros diviseis por 3 e 5 aparecer FizzBuzz no lugar do numero
# Numeros não diviseis por 3 e 5, aparecer o próprio número

def fizzbuzz(n):
    if n % 3 == 0 and n % 5 == 0:
        return 'fizzbuzz'
    elif n % 5 == 0:
        return 'buzz'
    elif n % 3 == 0:
        return 'fizz'
    else:
        return str(n)



l = []
for i in range(101):
    l.append(fizzbuzz(i))
print(l)